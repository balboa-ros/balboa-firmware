#include <Balboa32U4.h>
#include <Encoder.h>
#include <LSM6.h>
#include <PID_v1.h>
#include <Wire.h>
#include <ros.h>
#include <sensor_msgs/Imu.h>
#include <std_msgs/UInt64MultiArray.h>
#include <std_msgs/UInt16MultiArray.h>

Balboa32U4Encoders encoders;
LSM6 imu;
Balboa32U4Motors motors;

ros::NodeHandle n;

std_msgs::UInt64MultiArray enc_msg;
sensor_msgs::Imu imu_msg;

ros::Publisher enc_pub("enc", &enc_msg);
ros::Publisher imu_pub("imu", &imu_msg);

void motorCb(const std_msgs::UInt16MultiArray& motor_msg)
{
  motors.setLeftSpeed(motor_msg.data[0]);
  motors.setRightSpeed(motor_msg.data[1]);
}

ros::Subscriber<std_msgs::UInt16MultiArray> motor_sub("motor_val", &motorCb);

void setup()
{
  Wire.begin();
  imu.enableDefault();

  // Set the gyro full scale to 1000 dps because the default
  // value is too low, and leave the other settings the same.
  imu.writeReg(LSM6::CTRL2_G, 0b10001000);

  // Set the accelerometer full scale to 16 g because the default
  // value is too low, and leave the other settings the same.
  imu.writeReg(LSM6::CTRL1_XL, 0b10000100);

  n.initNode();
  n.advertise(enc_pub);
  n.advertise(imu_pub);
  n.subscribe(motor_sub);

  imu_msg.header.frame_id = "imu";
  uint64_t initial_enc[] = {0, 0};
  enc_msg.data = initial_enc;
}

void loop()
{
  enc_msg.data[0] += encoders.getCountsAndResetLeft();
  enc_msg.data[1] += encoders.getCountsAndResetRight();
  enc_pub.publish(&enc_msg);

  imu_msg.header.stamp = n.now();
  imu_msg.angular_velocity.x = imu.g.x;
  imu_msg.angular_velocity.y = imu.g.y;
  imu_msg.angular_velocity.z = imu.g.z;
  imu_msg.linear_acceleration.x = imu.a.x;
  imu_msg.linear_acceleration.y = imu.a.y;
  imu_msg.linear_acceleration.z = imu.a.z;

  n.spinOnce();
}
